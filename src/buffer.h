#ifndef BUFFER_H
#define BUFFER_H

#include <gtksourceviewmm/buffer.h>
#include <fstream>
#include <string>
#include <map>

class BufferManager
{
public:
    static BufferManager &Instance();

    Glib::RefPtr<Gsv::Buffer> &get(const std::string &filename);
    void save(const std::string &filename);

private:
    static BufferManager *instance;

    BufferManager();

    struct Buffer {
        Buffer(const std::string &filename);

        std::fstream fs;
        Glib::RefPtr<Gsv::Buffer> gb;
    };

    typedef std::map<std::string, Buffer*> BufferMap;
    BufferMap buffers;
};

#endif // BUFFER_H
