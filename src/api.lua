view =
{
    open_buffer = function(filename)
        pigmeu_open_buffer(filename)
    end,

    save_buffer = function(filename)
        if #filename == 0 then
            pigmeu_save_buffer()
        else
            pigmeu_save_buffer_as(filename)
        end
    end,

    close = function()
        pigmeu_close_view()
    end
}
