#include "term.h"

Term::Term()
{
    terminal_c = vte_terminal_new();

    char *argv[2] = {0, 0};
    argv[0] = vte_get_user_shell();

    char **envv = 0;

    // run the shell
    vte_terminal_fork_command_full(VTE_TERMINAL(terminal_c),
        VTE_PTY_DEFAULT, // how the shell is started
        0,  // working directory; set to NULL for default
        argv, // here include the strange construct
        envv, //NULL or another strange construct containing the environment variables
        (GSpawnFlags)(G_SPAWN_DO_NOT_REAP_CHILD | G_SPAWN_SEARCH_PATH),  //Spawnflags
        0, //setup function
        0, //pointer to child data
        0, //either NULL or reference to GPid object in this case (GPid pidterm;)
        0); //either NULL or reference to GError* object; important GError* must be set on 0 before using

    terminal_wrapped = Glib::wrap(terminal_c);

    add(*terminal_wrapped);
}

Term::~Term()
{
}
