#include "view.h"
#include "buffer.h"

View::View()
{
    add(view);

    show_all_children();
}

View::~View()
{

}

void View::set_buffer(const std::string &filename)
{
    this->filename = filename;
    view.set_buffer(BufferManager::Instance().get(filename));
}
