#ifndef TERM_H
#define TERM_H

#include <vte/vte.h>
#include <gtkmm/scrolledwindow.h>

class Term : public Gtk::ScrolledWindow
{
public:
    Term();
    virtual ~Term();

protected:
    Gtk::Widget *terminal_wrapped;
    GtkWidget *terminal_c;
};

#endif // TERM_H
