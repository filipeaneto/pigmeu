#include "control.h"
#include "pigmeu.h"

#include <fstream>

Control *Control::instance = 0;

Control &Control::Instance()
{
    if (!instance)
        instance = new Control();
    return *instance;
}

void Control::run_code(const std::string &command)
{
    try
    {
        context.executeCode(command);
    }
    catch(std::exception &e)
    {
        // TODO show error
        std::cerr << e.what() << '\n';
    }
}

Control::Control()
{
    // Writes core functions. TODO
    context.writeVariable("pigmeu_open_buffer", &Control::Open_buffer);

    // Loads API file.
    std::ifstream api("api.luac");
    context.executeCode(api);

    // Loads user configurations. TODO
    std::ifstream user("/home/filipeaneto/.pigmeu/config.lua");
    context.executeCode(user);
}

void Control::Open_buffer(std::string filename)
{
    Pigmeu &pigmeu = Pigmeu::Instance();

    pigmeu.change_current_buffer(filename);
}
