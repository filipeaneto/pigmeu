#ifndef COMMAND_H
#define COMMAND_H

#include <gtkmm/revealer.h>
#include <gtkmm/entry.h>

class Command : public Gtk::Revealer
{
public:
    Command();
    virtual ~Command();

protected:
    void run_command();
    bool lose_focus(GdkEventFocus *event);

    Gtk::Entry input;
};

#endif // COMMAND_H
