#ifndef CONTROL_H
#define CONTROL_H

#include "lua.h"

#include <string>

class Control
{
public:
    static Control &Instance();

    void run_code(const std::string &command);

private:
    Control();

    static Control *instance;

    // core functions
    static void Open_buffer(std::string filename);
    static void Save_buffer();
    static void Close_view();

    LuaContext context;
};

#endif // CONTROL_H
