#ifndef VIEW_H
#define VIEW_H

#include <gtksourceviewmm/buffer.h>
#include <gtkmm/scrolledwindow.h>
#include <gtksourceviewmm/view.h>
#include <string>

#include "term.h"

class View : public Gtk::ScrolledWindow
{
public:
    View();
    virtual ~View();

    void set_buffer(const std::string &filename);

private:
    std::string filename;
    Gsv::View view;
    Term term;
};

#endif // VIEW_H
