#include "buffer.h"
#include <boost/filesystem.hpp>

BufferManager *BufferManager::instance = 0;

BufferManager &BufferManager::Instance()
{
    if (!instance)
        instance = new BufferManager();
    return *instance;
}


Glib::RefPtr<Gsv::Buffer> &BufferManager::get(const std::string &filename)
{
    std::string path = boost::filesystem::absolute(filename).string();

    BufferMap::iterator it = buffers.find(path);

    if (it != buffers.end())
       return it->second->gb;

    Buffer *buffer = new Buffer(path);

    buffers[path] = buffer;

    return buffer->gb;
}

void BufferManager::save(const std::string &filename)
{
    // TODO
}

BufferManager::BufferManager()
{

}

BufferManager::Buffer::Buffer(const std::string &path) :
    fs(path), gb(Gsv::Buffer::create())
{
    fs.seekg(0, fs.end);
    long length = fs.tellg();
    fs.seekg(0, fs.beg);

    char *data = new char[length];
    fs.read(data, length);
    fs.seekg(0, fs.beg);

    gb->set_text(data);

    delete[] data;
}
