#ifndef PIGMEU_H
#define PIGMEU_H

#include "view.h"
#include "term.h"
#include "buffer.h"
#include "header.h"
#include "command.h"

#include <gtkmm/window.h>
#include <gtkmm/box.h>

class Pigmeu : public Gtk::Window
{
public:
    static Pigmeu &Instance();

    void toggle_command_bar();
    void change_current_buffer(const std::string &filename);

private:
    Pigmeu();
    virtual ~Pigmeu();

    // override events
    virtual bool on_key_press_event(GdkEventKey* event);

    static Pigmeu *instance;

    Gtk::Box layout;

    Header header;
    Command command;
    View view; // TODO use workspace manager (tabs and panes)
    //Workspace workspace;
    //Status status;
};

#endif // PIGMEU_H
