A lightweight graphical text editor designed for UNIX systems programmers.

Features:

* Lua plugins

* Pained and tabbed windows

* Embedded terminal emulator
