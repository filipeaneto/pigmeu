#include "pigmeu.h"
#include <gtkmm/application.h>

int main(int argc, char *argv[])
{
    Glib::RefPtr<Gtk::Application> app =
        Gtk::Application::create(argc, argv, "com.pigmeu");

    return app->run(Pigmeu::Instance());
}
