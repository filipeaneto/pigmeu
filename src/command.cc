#include "command.h"
#include "control.h"
#include "pigmeu.h"

Command::Command()
{
    input.signal_activate().connect(sigc::mem_fun(*this, &Command::run_command));
    input.signal_focus_out_event().connect(sigc::mem_fun(*this, &Command::lose_focus));

    add(input);
    set_reveal_child(false);
}

Command::~Command()
{
}

void Command::run_command()
{
    Control &control = Control::Instance();

    control.run_code(input.get_text());
    input.set_text("");
    Pigmeu::Instance().toggle_command_bar();
}

bool Command::lose_focus(GdkEventFocus * /* event */)
{
    set_reveal_child(false);
    return true;
}
