#ifndef HEADER_H
#define HEADER_H

#include <gtkmm/headerbar.h>

class Header : public Gtk::HeaderBar
{
public:
    Header();
    virtual ~Header();
};

#endif // HEADER_H
