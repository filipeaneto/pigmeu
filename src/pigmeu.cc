#include "pigmeu.h"

Pigmeu *Pigmeu::instance = 0;

Pigmeu &Pigmeu::Instance()
{
    if (!instance)
        instance = new Pigmeu();
    return *instance;
}

Pigmeu::Pigmeu() :
    layout(Gtk::ORIENTATION_VERTICAL)
{
    // overrides key events
    this->add_events(Gdk::KEY_PRESS_MASK);

    // setups title bar
    set_title("Pigmeu");
    set_default_geometry(800, 600);

    header.set_title("Pigmeu");
    header.set_show_close_button();

    set_titlebar(header);

    // setups command bar
    layout.pack_start(command, false, false);

    // setups workspace TODO
    layout.pack_start(view, true, true);

    // adds layout and shows its children
    add(layout);
    show_all_children();
    show_all();
}

Pigmeu::~Pigmeu()
{

}

void Pigmeu::toggle_command_bar()
{
    if (command.get_reveal_child())
    {
        command.set_reveal_child(false);
        set_focus(view);
    }
    else
    {
        command.set_reveal_child(true);
        set_focus(*command.get_child());
    }
}

void Pigmeu::change_current_buffer(const std::string &filename)
{
    view.set_buffer(filename);
}

bool Pigmeu::on_key_press_event(GdkEventKey* event)
{
    if (event->keyval == GDK_KEY_period &&
        (event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK | GDK_MOD1_MASK)) == GDK_CONTROL_MASK) {
        toggle_command_bar();
        return true;
    }

    return Gtk::Window::on_key_press_event(event);
}

